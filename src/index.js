import React from "react";
import ReactDOM from "react-dom";
import ReactBridPlayer from "./react-brid-player";

// const playlistObject = {"id":"0","mode":"latest"};
// For per channel feeds - {"id":"13","mode":"channel"}
// For per tag feeds - {"id":"sport","mode":"tag"}


ReactDOM.render(<ReactBridPlayer
  divId='Brid_12345'
  id='14260'
  width='640'
  height='360'
  //video='381222'
  playlist='5811'
/>, document.getElementById("root"));

if (module.hot) {
  module.hot.accept();
}
